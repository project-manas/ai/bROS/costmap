#include "costmap/inflation_layer.h"

#include <costmap/inflation_layer.h>
#include <pluginlib/class_list_macros.hpp>

namespace bROS {
namespace costmap {

InflationLayer::InflationLayer() { std::cout << "FINALLY" << std::endl; }

void InflationLayer::initialise(map_grid::f_point origin,
                                map_grid::i_point map_bounds, float resolution,
                                std::string frame) {
  std::cout << "init" << std::endl;
}
void InflationLayer::update_local() { std::cout << "local" << std::endl; }

} // namespace costmap
} // namespace bROS

PLUGINLIB_EXPORT_CLASS(bROS::costmap::InflationLayer, bROS::costmap::Layer)
