#include <costmap/map_layer.h>

namespace bROS {

namespace costmap {

MapLayer::MapLayer(map_grid::f_point origin, map_grid::i_point map_bounds,
                   float resolution, std::string frame)
    : Layer(origin, map_bounds, resolution, frame), Node("map_layer") {
  LOG("Map layer constructor");
}

void MapLayer::initialise() {
  LOG("Map layer initialised");
  std::string map_topic = "/map"; // get this from brosdb
  rate_ = 20;
  map_sub_ = this->create_subscription<nav_msgs::msg::OccupancyGrid>(
      map_topic,
      std::bind(&MapLayer::incoming_map, this, std::placeholders::_1));
  global_map_ = std::make_shared<map_grid::MapGrid>();
  std::thread spin_thread = std::thread(&MapLayer::spin_loop, this);
  spin_thread.detach();

  is_init_ = true;
}

void MapLayer::update_local(float delta_x, float delta_y) {
  LOG("MapLayer::update_local");
  if (!is_init_) {
    LOG("Map layer uninitialised");
    return;
  }

  map_->move(delta_x, delta_y);

  // Aligning the maps
  // g++ -O3 optims the func calls
  map_grid::f_point global_min_point = global_map_->map_to_world(0, 0);
  map_grid::f_point global_max_point =
      global_map_->map_to_world(global_map_->map_bounds_);

  map_grid::f_point local_min_point = map_->map_to_world(0, 0);
  map_grid::f_point local_max_point = map_->map_to_world(map_->map_bounds_);

  map_grid::f_point min_point = {
      std::max(global_min_point.x, local_min_point.x),
      std::max(global_min_point.y, local_min_point.y)};

  map_grid::f_point max_point = {
      std::min(global_max_point.x, local_max_point.x),
      std::min(global_max_point.y, local_max_point.y)};

  map_grid::i_point local_min_map_point = map_->world_to_map(min_point);
  map_grid::i_point local_max_map_point = map_->world_to_map(max_point);
  map_grid::i_point global_min_map_point = global_map_->world_to_map(min_point);
  map_grid::i_point global_max_map_point = global_map_->world_to_map(max_point);

  map_grid::i_point delta_local = {
      local_max_map_point.x - local_min_map_point.x,
      local_max_map_point.y - local_min_map_point.y};

  map_grid::i_point delta_global = {
      global_max_map_point.x - global_min_map_point.x,
      global_max_map_point.y - global_min_map_point.y};

  // delta_local and delta_global are approx. equal (fp error)
  for (int i = 0; i < delta_local.x; ++i) {
    for (int j = 0; j < delta_local.y; ++j) {
      int local_idx =
          map_->get_index(local_min_map_point.x + i, local_max_map_point.y = j);
      int global_idx = global_map_->get_index(global_min_map_point.x + i,
                                              global_min_map_point.y + j);
      (*map_)[local_idx] = (*global_map_)[global_idx];
    }
  }
}

void MapLayer::incoming_map(const nav_msgs::msg::OccupancyGrid::SharedPtr map) {
  LOG("MapLayer::incoming_map");

  // TODO (Squadrick): Using unaligned fromROSMsg, slow interpolation causes
  // double-free error
  global_map_->fromROSMsg(map, true);
  global_map_->change_resolution(map_->resolution_, true);
  global_map_->_display_info();
}

void MapLayer::spin_loop() {
  rclcpp::Rate rate(rate_);
  while (rclcpp::ok()) {
    rclcpp::spin_some(this->get_node_base_interface());
    rate.sleep();
  }
}

} // namespace costmap

} // namespace bROS
