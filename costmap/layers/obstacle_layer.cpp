#include <costmap/obstacle_layer.h>

#define SIGN(x) ((x) < 0.0 ? -1.0 : 1.0)
#define FOR_EACH(it, con)                                                      \
  for (auto(it) = (con).begin(); (it) != (con).end(); ++(it))

namespace bROS {
namespace costmap {
ObstacleLayer::ObstacleLayer(map_grid::f_point origin,
                             map_grid::i_point map_bounds, float resolution,
                             std::string frame)
    : Layer(origin, map_bounds, resolution, frame), Node("obstacle_layer"),
      ros_clock_(RCL_ROS_TIME) {
  LOG("Obstacle layer constructor");
}

ObstacleLayer::~ObstacleLayer() {
  while (!clearing_buffers_.empty())
    clearing_buffers_.pop_back();

  while (!marking_buffers_.empty())
    marking_buffers_.pop_back();

  while (!observation_buffers_.empty())
    observation_buffers_.pop_back();

  while (!observation_subscriptions_.empty())
    observation_subscriptions_.pop_back();
}

void ObstacleLayer::initialise() {

  // TODO (squadrick): Add brosdb support
  std::vector<std::string> topic_names = {"/pointcloud"};

  for (auto it : topic_names) {
    double observation_keep_time = 0.0;
    double expected_update_rate = 0.0;
    double min_obstacle_height = 0.0;
    double max_obstacle_height = 2.0;
    double obstacle_range = 2.5;
    double raytrace_range = 3.0;
    bool clearing = true;
    bool marking = true;
    std::string sensor_frame;

    auto observation_buffer = std::make_shared<ObservationBuffer>(
        it, observation_keep_time, expected_update_rate, min_obstacle_height,
        max_obstacle_height, obstacle_range, raytrace_range, *tf_buffer_,
        global_frame_, sensor_frame, &ros_clock_);

    observation_buffers_.push_back(observation_buffer);

    if (clearing) {
      clearing_buffers_.push_back(observation_buffers_.back());
    }

    if (marking) {
      marking_buffers_.push_back(observation_buffers_.back());
    }

    // NOTE: Hack fix for https://github.com/ros2/rclcpp/issues/273
    std::function<void(const sensor_msgs::msg::PointCloud2::SharedPtr)> pcl_cb =
        std::bind(&ObstacleLayer::incoming_pcl, this, std::placeholders::_1,
                  observation_buffer);
    observation_subscriptions_.push_back(
        this->create_subscription<sensor_msgs::msg::PointCloud2>(it, pcl_cb));
  }
  std::thread spin_thread = std::thread(&ObstacleLayer::spin_loop, this);
  spin_thread.detach();
  is_init_ = true;
}

void ObstacleLayer::spin_loop() {
  rclcpp::Rate rate(10.0);
  while (rclcpp::ok()) {
    rclcpp::spin_some(this->get_node_base_interface());
    rate.sleep();
  }
}

void ObstacleLayer::incoming_pcl(
    const sensor_msgs::msg::PointCloud2::SharedPtr pointcloud,
    const std::shared_ptr<ObservationBuffer> buffer) {
  buffer->buffer_cloud(*pointcloud);
}

void ObstacleLayer::update_local(float delta_x, float delta_y) {
  if (!is_init_) {
    return;
  }

  map_->move(delta_x, delta_y);
  std::vector<Observation> clearing_observations, marking_observations;

  FOR_EACH(buffer, clearing_buffers_)
  (*buffer)->get_observations(clearing_observations);
  FOR_EACH(buffer, marking_buffers_)
  (*buffer)->get_observations(marking_observations);

  FOR_EACH(observation, clearing_observations)
  raytrace_free_space(&*observation);
  // NOTE: Don't convert `&*observation` to `observation`, it won't work

  FOR_EACH(observation, marking_observations) {
    auto square =
        std::bind((double (*)(double, int))std::pow, std::placeholders::_1, 2);

    sensor_msgs::msg::PointCloud2 cloud = *(observation->cloud_);
    sensor_msgs::PointCloud2Iterator<float> iter_xyz(cloud, "xyz");

    for (; iter_xyz != iter_xyz.end(); ++iter_xyz) {
      double px = iter_xyz[0];
      double py = iter_xyz[1];
      double pz = iter_xyz[2];

      double sq_ob_dist = square(px - observation->origin_.x) +
                          square(py - observation->origin_.y) +
                          square(pz - observation->origin_.z);

      if (sq_ob_dist > square(observation->obstacle_range_))
        continue; // Point is too far away

      map_grid::i_point mark_point = map_->world_to_map(px, py);
      (*map_)[map_->get_index(mark_point.x, mark_point.y)] = 100;
    }
  }
}

void ObstacleLayer::raytrace_free_space(
    const Observation *clearing_observation) {
  sensor_msgs::msg::PointCloud2 &cloud = *(clearing_observation->cloud_);
  double ox = clearing_observation->origin_.x;
  double oy = clearing_observation->origin_.y;

  map_grid::i_point map_point = map_->world_to_map(ox, oy);

  sensor_msgs::PointCloud2Iterator<float> iter_x(cloud, "x");
  sensor_msgs::PointCloud2Iterator<float> iter_y(cloud, "y");

  map_grid::f_point origin = map_->origin_;
  map_grid::f_point map_end = map_->map_to_world(map_->map_bounds_);

  for (; iter_x != iter_x.end(); ++iter_x, ++iter_y) {
    double wx = *iter_x;
    double wy = *iter_y;

    // now we also need to make sure that the endpoint we're raytracing
    // to isn't off the costmap and scale if necessary
    double a = wx - ox;
    double b = wy - oy;

    // the minimum value to raytrace from is the origin
    if (wx < origin.x) {
      double t = (origin.x - ox) / a;
      wx = origin.x;
      wy = oy + b * t;
    }
    if (wy < origin.y) {
      double t = (origin.y - oy) / b;
      wx = ox + a * t;
      wy = origin.y;
    }

    // the maximum value to raytrace to is the end of the map
    if (wx > map_end.x) {
      double t = (map_end.x - ox) / a;
      wx = map_end.x - .001;
      wy = oy + b * t;
    }
    if (wy > map_end.y) {
      double t = (map_end.y - oy) / b;
      wx = ox + a * t;
      wy = map_end.y - .001;
    }

    map_grid::i_point end_point = map_->world_to_map(wx, wy);
    auto raytrace_range = static_cast<uint32_t>(
        clearing_observation->raytrace_range_ / map_->resolution_);
    raytrace_line(map_point, end_point, raytrace_range);
  }
}

void ObstacleLayer::raytrace_line(map_grid::i_point start,
                                  map_grid::i_point end, uint32_t max_length) {

  int dx = end.x - start.x;
  int dy = end.y - start.y;

  auto abs_dx = static_cast<uint32_t>(abs(dx));
  auto abs_dy = static_cast<uint32_t>(abs(dy));

  int offset_dx, offset_dy;
  offset_dx = static_cast<int>(SIGN(dx));
  offset_dy = static_cast<int>(SIGN(dy) * map_->get_width());

  int32_t offset = map_->get_index(start.x, start.y);

  // we need to chose how much to scale our dominant dimension, based on the
  // maximum length of the line
  double dist = hypot(dx, dy);
  double scale = (dist == 0.0) ? 1.0 : std::min(1.0, max_length / dist);

  if (abs_dx >= abs_dy) {
    // if x is dominant
    int error_y = abs_dx / 2;
    bresenham(abs_dx, abs_dy, error_y, offset_dx, offset_dy, offset,
              (unsigned int)(scale * abs_dx));
  } else {
    // y is dominant
    int error_x = abs_dy / 2;
    bresenham(abs_dy, abs_dx, error_x, offset_dy, offset_dx, offset,
              (unsigned int)(scale * abs_dy));
  }
}

void ObstacleLayer::bresenham(uint32_t abs_da, uint32_t abs_db, int error_b,
                              int offset_a, int offset_b, int offset,
                              uint32_t max_length) {
  unsigned int end = std::min(max_length, abs_da);
  for (unsigned int i = 0; i < end; ++i) {
    (*map_)[offset] = 0;
    offset += offset_a;
    error_b += abs_db;
    if ((unsigned int)error_b >= abs_da) {
      offset += offset_b;
      error_b -= abs_da;
    }
  }
  (*map_)[offset] = 0;
}

} // namespace costmap

} // namespace bROS
