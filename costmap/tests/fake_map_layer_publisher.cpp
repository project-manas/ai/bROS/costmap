//
// Created by squadrick on 2/12/19.
//


#include <rclcpp/rclcpp.hpp>
#include <nav_msgs/msg/occupancy_grid.hpp>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include <tf2/LinearMath/Quaternion.h>

#include "map_grid/map_grid.h"

class MapGridPublisher : public rclcpp::Node {
 public:
  MapGridPublisher() :
    Node("fake_map_layer_publisher"),
    global_frame("map_fixed_id"),
    map_frame("map_id"),
    map_grid_({0, 0}, {100, 200}, 0.01) {

    count = 0;
    move_x = 0.2;
    move_y = 0.1;
    map_grid_.set_all(0);

    for (int i = 20; i < 70; ++i) {
      for (int j = 30; j < 80; ++j) {
        map_grid_[map_grid_.get_index(i, j)] = 100;
      }
    }

    publisher_ = this->create_publisher<nav_msgs::msg::OccupancyGrid>("/map");
    br = std::make_shared<tf2_ros::TransformBroadcaster>(std::shared_ptr<MapGridPublisher>(this));

    RCLCPP_INFO(this->get_logger(), "MapGridPublisher created");
  }

  void publish_map() {
    count++;
    auto message = nav_msgs::msg::OccupancyGrid();
    map_grid_.toROSMsg(message);

    //map_grid_.move(move_x, move_y);



    geometry_msgs::msg::TransformStamped transform;
    transform.header.stamp = this->get_clock()->now();
    transform.header.frame_id = global_frame;
    transform.child_frame_id = map_frame;
    transform.transform.translation.x = 0.0;
    transform.transform.translation.y = 0.0;
    transform.transform.translation.z = 0.0;
    tf2::Quaternion q;
    q.setRPY(1, 0, 0);
    transform.transform.rotation.x = q.x();
    transform.transform.rotation.y = q.y();
    transform.transform.rotation.z = q.z();
    transform.transform.rotation.w = q.w();
    br->sendTransform(transform);


    message.header.stamp = this->get_clock()->now();
    message.header.frame_id = map_frame;
    publisher_->publish(message);

    //rclcpp::sleep_for(std::chrono::seconds(2));

  }

 private:
  bROS::map_grid::MapGrid map_grid_;
  rclcpp::Publisher<nav_msgs::msg::OccupancyGrid>::SharedPtr publisher_;
  std::shared_ptr<tf2_ros::TransformBroadcaster> br;
  uint32_t count;

  std::string map_frame, global_frame;

  float move_x, move_y;
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<MapGridPublisher>();
  while (rclcpp::ok()) {
    node->publish_map();
  }
  rclcpp::shutdown();
  return 0;
}
