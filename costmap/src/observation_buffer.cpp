#include <utility>

#include "costmap/observation_buffer.h"

namespace bROS {

namespace costmap {

ObservationBuffer::ObservationBuffer(
    std::string topic_name, int32_t observation_keep_time,
    int32_t expected_update_rate, double min_obstacle_height,
    double max_obstacle_height, double obstacle_range, double raytrace_range,
    tf2_ros::Buffer &tf_buffer, std::string global_frame,
    std::string sensor_frame, rclcpp::Clock *ros_clock)
    : topic_name_(std::move(topic_name)),
      observation_keep_time_(observation_keep_time, 0),
      expected_update_rate_(expected_update_rate, 0),
      min_obstacle_height_(min_obstacle_height),
      max_obstacle_height_(max_obstacle_height),
      obstacle_range_(obstacle_range), raytrace_range_(raytrace_range),
      tf_buffer_(tf_buffer), global_frame_(std::move(global_frame)),
      sensor_frame_(std::move(sensor_frame)) {
  ros_clock_ = ros_clock;
}

ObservationBuffer::~ObservationBuffer() {
  ros_clock_ = nullptr;
  observation_list_.clear();
}

void ObservationBuffer::buffer_cloud(
    const sensor_msgs::msg::PointCloud2 &cloud) {
  Observation observation(obstacle_range_, raytrace_range_);
  std::string origin_frame =
      sensor_frame_.empty() ? cloud.header.frame_id : sensor_frame_;
  sensor_msgs::msg::PointCloud2 global_frame_cloud;
  geometry_msgs::msg::PointStamped global_origin, local_origin;

  local_origin.header.stamp = global_frame_cloud.header.stamp =
      cloud.header.stamp;
  local_origin.header.frame_id = origin_frame;

  local_origin.point.x = 0;
  local_origin.point.y = 0;
  local_origin.point.z = 0;

  try {
    //    tf_buffer_.transform(local_origin, global_origin, global_frame_);
    //    tf2::convert(global_origin.point, observation.origin_);
    //    tf_buffer_.transform(cloud, global_frame_cloud, global_frame_);
  }

  catch (tf2::TransformException &ex) {
    std::cerr << ex.what() << std::endl;
    return;
  }

  // TODO (squadrick): PCL does a memcpy, compare speed with non-memcpy method
  pcl::PointCloud<pcl::PointXYZ>::Ptr unfiltered_cloud(
      new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr filtered_cloud(
      new pcl::PointCloud<pcl::PointXYZ>);
  pcl::fromROSMsg(global_frame_cloud, *unfiltered_cloud);
  pcl::PassThrough<pcl::PointXYZ> height_filter;
  height_filter.setInputCloud(unfiltered_cloud);
  height_filter.setFilterFieldName("z");
  height_filter.setFilterLimits(static_cast<float>(min_obstacle_height_),
                                static_cast<float>(max_obstacle_height_));
  height_filter.filter(*filtered_cloud);
  pcl::toROSMsg(*filtered_cloud, *observation.cloud_);

  observation.cloud_->header = cloud.header;
  observation.cloud_->height = global_frame_cloud.height;
  observation.cloud_->width = global_frame_cloud.width;
  observation.cloud_->fields = global_frame_cloud.fields;
  observation.cloud_->is_bigendian = global_frame_cloud.is_bigendian;
  observation.cloud_->point_step = global_frame_cloud.point_step;
  observation.cloud_->row_step = global_frame_cloud.row_step;
  observation.cloud_->is_dense = global_frame_cloud.is_dense;

  std::lock_guard<std::mutex> guard(mut);
  observation_list_.push_front(observation);
  last_updated_ = ros_clock_->now();
  purge_old_observations();
}

void ObservationBuffer::get_observations(
    std::vector<Observation> &observations) {
  std::lock_guard<std::mutex> guard(mut);
  purge_old_observations();

  for (const auto &obs_it : observation_list_)
    observations.push_back(obs_it);
}

void ObservationBuffer::purge_old_observations() {
  if (observation_list_.empty())
    return;

  for (auto obs = observation_list_.begin(); obs != observation_list_.end();
       ++obs) {
    if ((last_updated_ - obs->cloud_->header.stamp) > observation_keep_time_) {
      observation_list_.erase(obs, observation_list_.end());
      return;
    }
  }
}

bool ObservationBuffer::is_current() const {
  if (expected_update_rate_.nanoseconds() == 0)
    return true;

  return (ros_clock_->now() - last_updated_) <= expected_update_rate_;
}

} // namespace costmap

} // namespace bROS