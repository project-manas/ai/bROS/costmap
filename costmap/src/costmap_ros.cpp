#include "costmap/costmap_ros.h"

#define PUBLISH_DELAY_THRESHOLD 5e-2
#define UPDATE_DELAY_THRESHOLD 5e-2

using namespace std::chrono_literals;

#define CREATE_LAYER(v, l)                                                     \
  v.push_back(std::make_shared<l>(origin, bounds, resolution_, global_frame_))

namespace bROS {

namespace costmap {

CostmapROS::CostmapROS() : Node("costmap_ros") {

  // TODO (Squadrick): Use bROSdb
  global_frame_ = "map";
  base_frame_ = "base_link";
  size_x_ = size_y_ = 200;
  resolution_ = 1.0;
  default_cost_ = 0;
  min_update_freq_ = 2.0;
  update_freq_ = min_update_freq_;
  updated_ = false;
  transform_tolerance_ = 1.0;
  min_publish_freq_ = 1.0;
  publish_freq_ = min_publish_freq_;
  map_publish_thread_shutdown_ = false;
  odom_topic_ = "/odom";
  vel_init = false;
  pub_topic_ = "/costmap";

  std::vector<std::shared_ptr<Layer>> layers;
  map_grid::f_point origin = {0.0, 0.0};
  map_grid::i_point bounds = {size_x_, size_y_};

  // Add new layers here
  CREATE_LAYER(layers, MapLayer);
  CREATE_LAYER(layers, ObstacleLayer);
//  CREATE_LAYER(layers, InflationLayer);

  costmap_ = new Costmap(layers);
  publisher_ = new CostmapPublisher(pub_topic_);
  buffer_ = std::make_shared<tf2_ros::Buffer>(get_clock(), duration);

  subscription_ = this->create_subscription<nav_msgs::msg::Odometry>(
      odom_topic_,
      std::bind(&CostmapROS::odomCallback, this, std::placeholders::_1));

  map_update_thread_ = std::thread(&CostmapROS::mapUpdateLoop, this);
  map_publish_thread_ = std::thread(&CostmapROS::mapPublishLoop, this);
}

CostmapROS::~CostmapROS() {
  delete costmap_;
  delete publisher_;
  map_publish_thread_shutdown_ = true;
  map_update_thread_.join();
  map_publish_thread_.join();
}

void CostmapROS::mapUpdateLoop() {
  rclcpp::Time current;
  updated_ = false;
  while (rclcpp::ok()) {
    rclcpp::Rate rate(update_freq_);
    rclcpp::Time start = get_clock()->now();
    if (update_freq_ <= 0.0) {
      RCLCPP_WARN(this->get_logger(),
                  "Costmap update frequency is set to %f. Skipping update loop",
                  min_publish_freq_);
      return;
    }
    current = get_clock()->now();
    mapUpdate();
    updated_ = true;
    rclcpp::Time finish = get_clock()->now();
    rate.sleep();
    double time_taken = (finish.nanoseconds() - start.nanoseconds()) / 1e9;
    double update_delay = time_taken - (1.0 / update_freq_);

    if (update_delay > UPDATE_DELAY_THRESHOLD) {
      RCLCPP_WARN(this->get_logger(),
                  "Costmap update loop failed. Desired frequency is %fHz. The "
                  "loop actually took %f seconds",
                  update_freq_, time_taken);
    }
  }
}

void CostmapROS::mapUpdate() {
  geometry_msgs::msg::PoseStamped pose;
  pose.pose = odom_.pose.pose;
  if (getRobotPose(pose)) {
    costmap_->update(pose.pose);
    publisher_->prepareMap(*costmap_);
  }
}

bool CostmapROS::getRobotPose(geometry_msgs::msg::PoseStamped &pose) {
  pose.header.frame_id = base_frame_;
  pose.header.stamp = get_clock()->now();
  rclcpp::Time start = get_clock()->now();
  try {
    rclcpp::Time time = rclcpp::Time(0); // for the latest transform

    buffer_->transform(pose, pose, "odom", tf2::durationFromSec(1.0));
    tf2::TimePoint tf2_time(std::chrono::nanoseconds(time.nanoseconds()));
    geometry_msgs::msg::TransformStamped tfp =
        buffer_->lookupTransform("odom", global_frame_, tf2_time);
    tf2::doTransform(pose, pose, tfp);
  } catch (tf2::TransformException &ex) {
    RCLCPP_WARN(this->get_logger(), "%s", ex.what());
    return false;
  }
  rclcpp::Time finish = ros_clock_->now();
  if (RCL_NS_TO_S(finish.nanoseconds()) - RCL_NS_TO_S(start.nanoseconds()) >
      transform_tolerance_) {
    RCLCPP_WARN(this->get_logger(),
                "Costmap %s to %s transform timed out. Current time: %d, "
                "global_pose stamp %d, tolerance %d",
                global_frame_.c_str(), base_frame_.c_str(),
                RCL_NS_TO_S(finish.nanoseconds()), pose.header.stamp,
                transform_tolerance_);
  }
  return true;
}

void CostmapROS::mapPublishLoop() {
  rclcpp::Rate rate(publish_freq_);
  rclcpp::Time current;
  while (rclcpp::ok()) {
    if (!map_publish_thread_shutdown_) {
      rclcpp::Time start = ros_clock_->now();
      if (publish_freq_ <= 0.0) {
        RCLCPP_WARN(
            this->get_logger(),
            "Costmap publish frequency is set to %f. Skipping publish loop.",
            min_publish_freq_);
        return;
      }
      current = ros_clock_->now();
      while (!updated_) {
        RCLCPP_WARN(this->get_logger(),
                    "Costmap publish loop failed. Desired frequency is %fHz.",
                    min_publish_freq_);
        rclcpp::sleep_for(std::chrono::seconds(1));
      }
      publisher_->publish();
      updated_ = false;

      rclcpp::Time finish = ros_clock_->now();
      rate.sleep();
      double time_taken = (finish.nanoseconds() - start.nanoseconds()) / 1e9;
      double publish_delay = time_taken - 1.0 / min_publish_freq_;
      if (publish_delay > PUBLISH_DELAY_THRESHOLD) {
        RCLCPP_WARN(this->get_logger(),
                    "Costmap publish loop failed. Desired frequency is %fHz. "
                    "The loop actually took %f seconds",
                    publish_freq_, time_taken);
      }
    } else {
      RCLCPP_INFO(this->get_logger(), "Shutting down costmap...");
      break;
    }
  }
}

void CostmapROS::odomCallback(const nav_msgs::msg::Odometry::SharedPtr msg) {
  odom_ = *msg;
  vel_init = true;
}

} // namespace costmap

} // namespace bROS
