#include "costmap/costmap_ros.h"

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  auto costmap_node = std::make_shared<bROS::costmap::CostmapROS>();
  rclcpp::spin(costmap_node);
  rclcpp::shutdown();
  return 0;
}
