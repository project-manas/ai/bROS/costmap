#include "costmap/costmap_publisher.h"

namespace bROS {

namespace costmap {

CostmapPublisher::CostmapPublisher(std::string topic)
    : Node("costmap_publisher"), topic_(topic) {
  publisher_ = this->create_publisher<nav_msgs::msg::OccupancyGrid>(topic_);
  costmap_translation_table_[0] = 0;     // NO obstacle
  costmap_translation_table_[253] = 99;  // INSCRIBED obstacle
  costmap_translation_table_[254] = 100; // LETHAL obstacle
  costmap_translation_table_[255] = -1;  // UNKNOWN

  for (int i = 1; i < 253; i++)
    costmap_translation_table_[i] = char(1 + (97 * (i - 1)) / 251);
}

CostmapPublisher::~CostmapPublisher() = default;

void CostmapPublisher::publish() { publisher_->publish(map_); }

void CostmapPublisher::prepareMap(Costmap &costmap) {
  costmap.master_grid_->lock();
  for (uint32_t i = 0; i < costmap.master_grid_->get_width(); ++i) {
    for (uint32_t j = 0; j < costmap.master_grid_->get_height(); ++j) {
      int idx = costmap.master_grid_->get_index(i, j);
      (*costmap.master_grid_)[idx] =
          costmap_translation_table_[(*costmap.master_grid_)[idx]];
    }
  }
  costmap.master_grid_->toROSMsg(map_);
  costmap.master_grid_->unlock();
}

} // namespace costmap

} // namespace bROS