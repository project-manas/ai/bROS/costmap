#include <costmap/costmap.h>

#include "costmap/costmap.h"

namespace bROS {
namespace costmap {

Costmap::Costmap(std::vector<std::shared_ptr<Layer>> &layers)
    : layers_(layers) {
  for (auto &layer : layers_)
    layer->initialise();
}

void Costmap::update(const geometry_msgs::msg::Pose &origin) {
  LOG("Costmap::update");
  float delta_x, delta_y; // change in x, y co-ordinate since last update

  if (!origin_init_) {
    delta_x = 0, delta_y = 0;
  } else {
    delta_x = static_cast<float>(origin.position.x - prev_origin_.position.x);
    delta_y = static_cast<float>(origin.position.y - prev_origin_.position.y);
  }

  for (auto &plugin : layers_) {
    plugin->update_local(delta_x, delta_y);
  }

  for (auto &plugin : layers_) {
    plugin->update_global(master_grid_);
  }

  master_grid_->origin_.x = static_cast<float>(origin.position.x);
  master_grid_->origin_.y = static_cast<float>(origin.position.y);

  origin_init_ = true;
  prev_origin_ = origin;
}

} // namespace costmap

} // namespace bROS
