#include <costmap/layer.h>

#include "costmap/layer.h"

namespace bROS {

namespace costmap {

Layer::Layer(map_grid::f_point origin, map_grid::i_point map_bounds,
             float resolution, std::string frame) {
  map_ = std::make_shared<map_grid::MapGrid>(origin, map_bounds, resolution,
                                             frame);
}

void Layer::initialise() { is_init_ = true; }

void Layer::update_global(std::shared_ptr<map_grid::MapGrid> global_map) {
  LOG("update global start");

#ifdef UPDATE_AVG
  LOG("Avg update");
  global_map->update_with_avg(*map_);

#else
  LOG("Max update");
  global_map->update_with_max(*map_);

#endif

  LOG("update global end");
}

} // namespace costmap

} // namespace bROS
