#ifndef COSTMAP_COSTMAP_ROS_H_
#define COSTMAP_COSTMAP_ROS_H_

#include <chrono>
#include <cmath>
#include <memory>
#include <string>

#include "geometry_msgs/msg/point_stamped.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "pluginlib/class_loader.hpp"
#include "pluginlib/exceptions.hpp"
#include "rclcpp/rclcpp.hpp"
#include "tf2/utils.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/transform_listener.h"
#include <costmap/costmap.h>
#include <costmap/costmap_publisher.h>
#include <costmap/layer.h>
#include <costmap/map_layer.h>
#include <costmap/obstacle_layer.h>
#include <map_grid/map_grid.h>

namespace bROS {
namespace costmap {
class CostmapROS : public rclcpp::Node {
public:
  CostmapROS();

  virtual ~CostmapROS();

protected:
  Costmap *costmap_;

private:
  void mapUpdateLoop();

  void mapUpdate();

  bool getRobotPose(geometry_msgs::msg::PoseStamped &pose);

  void mapPublishLoop();

  void odomCallback(nav_msgs::msg::Odometry::SharedPtr msg);

  std::string global_frame_, base_frame_;
  int32_t size_x_, size_y_;
  float resolution_;
  unsigned char default_cost_;

  tf2::Duration duration = tf2::Duration(std::chrono::seconds(1));
  std::shared_ptr<tf2_ros::Buffer> buffer_;

  rclcpp::Clock::SharedPtr ros_clock_;

  float min_update_freq_, update_freq_;
  std::thread map_update_thread_;
  bool updated_;
  float transform_tolerance_;

  float min_publish_freq_, publish_freq_;
  std::thread map_publish_thread_;
  bool map_publish_thread_shutdown_;
  CostmapPublisher *publisher_;

  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subscription_;
  nav_msgs::msg::Odometry odom_;
  std::string odom_topic_;
  bool vel_init;

  std::string pub_topic_;
};

} // namespace costmap

} // namespace bROS

#endif // COSTMAP_COSTMAP_ROS_H_
