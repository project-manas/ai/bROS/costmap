#ifndef OBSERVATION_BUFFER_H_
#define OBSERVATION_BUFFER_H_

#include <mutex>

#include <geometry_msgs/msg/point.hpp>
#include <rclcpp/clock.hpp>
#include <rclcpp/rclcpp.hpp>
#include <rclcpp/time.hpp>
#include <sensor_msgs/msg/point_cloud2.hpp>
#include <sensor_msgs/point_cloud2_iterator.hpp>

#include <pcl/conversions.h>
#include <pcl/filters/passthrough.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

#include <tf2/utils.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/buffer.h>

namespace bROS {

namespace costmap {

class Observation {
public:
  Observation()
      : cloud_(std::make_shared<sensor_msgs::msg::PointCloud2>()),
        obstacle_range_(0.0), raytrace_range_(0.0) {}

  Observation(geometry_msgs::msg::Point &origin,
              const sensor_msgs::msg::PointCloud2 &cloud, double obstacle_range,
              double raytrace_range)
      : origin_(origin),
        cloud_(std::make_shared<sensor_msgs::msg::PointCloud2>(cloud)),
        obstacle_range_(obstacle_range), raytrace_range_(raytrace_range) {}

  Observation(const Observation &obs)
      : origin_(obs.origin_),
        cloud_(std::make_shared<sensor_msgs::msg::PointCloud2>(*(obs.cloud_))),
        obstacle_range_(obs.obstacle_range_),
        raytrace_range_(obs.raytrace_range_) {}

  Observation(const sensor_msgs::msg::PointCloud2 &cloud, double obstacle_range)
      : cloud_(std::make_shared<sensor_msgs::msg::PointCloud2>(cloud)),
        obstacle_range_(obstacle_range), raytrace_range_(0.0) {}

  Observation(double obstacle_range, double raytrace_range)
      : cloud_(std::make_shared<sensor_msgs::msg::PointCloud2>()),
        obstacle_range_(obstacle_range), raytrace_range_(raytrace_range) {}

  geometry_msgs::msg::Point origin_;
  std::shared_ptr<sensor_msgs::msg::PointCloud2> cloud_;
  double obstacle_range_, raytrace_range_;
};

class ObservationBuffer {
public:
  ObservationBuffer(std::string topic_name, int32_t observation_keep_time,
                    int32_t expected_update_rate, double min_obstacle_height,
                    double max_obstacle_height, double obstacle_range,
                    double raytrace_range, tf2_ros::Buffer &tf_buffer,
                    std::string global_frame, std::string sensor_frame,
                    rclcpp::Clock *ros_clock);

  ~ObservationBuffer();

  bool is_current() const;
  void buffer_cloud(const sensor_msgs::msg::PointCloud2 &cloud);
  void get_observations(std::vector<Observation> &observations);

private:
  double min_obstacle_height_, max_obstacle_height_, obstacle_range_,
      raytrace_range_;
  rclcpp::Clock *ros_clock_;
  rclcpp::Duration observation_keep_time_, expected_update_rate_;
  rclcpp::Time last_updated_;
  std::mutex mut;
  std::list<Observation> observation_list_;
  std::string topic_name_, global_frame_, sensor_frame_;
  tf2_ros::Buffer &tf_buffer_;

  void purge_old_observations();
};

} // namespace costmap

} // namespace bROS

#endif // OBSERVATION_BUFFER_H_
