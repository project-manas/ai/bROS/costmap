#ifndef COSTMAP_INFLATION_LAYER_H_
#define COSTMAP_INFLATION_LAYER_H_

#include "costmap/layer.h"

namespace bROS {

namespace costmap {

class InflationLayer : public Layer {
public:
  InflationLayer();

  virtual void initialise(map_grid::f_point origin,
                          map_grid::i_point map_bounds, float resolution,
                          std::string frame);

  virtual void update_local();
};

} // namespace costmap

} // namespace bROS
#endif // COSTMAP_INFLATION_LAYER_H_
