#ifndef COSTMAP_COSTMAP_H_
#define COSTMAP_COSTMAP_H_

#include <memory>
#include <string>
#include <vector>

#include "geometry_msgs/msg/pose.hpp"
#include "rclcpp/rclcpp.hpp"

#include "costmap/layer.h"

namespace bROS {
namespace costmap {
class Costmap {
public:
  explicit Costmap(std::vector<std::shared_ptr<Layer>> &layers);

  void update(const geometry_msgs::msg::Pose &);

  bool origin_init_;

  std::shared_ptr<map_grid::MapGrid> master_grid_;

private:
  std::vector<std::shared_ptr<Layer>> layers_;

  geometry_msgs::msg::Pose prev_origin_;
};

} // namespace costmap

} // namespace bROS

#endif // COSTMAP_COSTMAP_H_
