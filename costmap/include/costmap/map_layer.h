#ifndef COSTMAP_MAP_LAYER_H_
#define COSTMAP_MAP_LAYER_H_

#include <nav_msgs/msg/occupancy_grid.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/string.hpp>

#include "costmap/layer.h"

namespace bROS {

namespace costmap {

class MapLayer : public costmap::Layer, rclcpp::Node {
public:
  MapLayer(map_grid::f_point origin, map_grid::i_point map_bounds,
           float resolution, std::string frame);

  void initialise() override;

private:
  void incoming_map(nav_msgs::msg::OccupancyGrid::SharedPtr map);
  void spin_loop();
  void update_local(float delta_x, float delta_y) override;

  double rate_;
  rclcpp::Subscription<nav_msgs::msg::OccupancyGrid>::SharedPtr map_sub_;
  std::shared_ptr<map_grid::MapGrid> global_map_;
};

} // namespace costmap

} // namespace bROS

#endif // COSTMAP_MAP_LAYER_H_
