#ifndef COSTMAP_OBSTACLE_LAYER_H_
#define COSTMAP_OBSTACLE_LAYER_H_

#include "costmap/layer.h"
#include "costmap/observation_buffer.h"
#include "map_grid/map_grid.h"
#include <cmath>
#include <nav_msgs/msg/odometry.hpp>

namespace bROS {
namespace costmap {
class ObstacleLayer : public Layer, rclcpp::Node {
public:
  ObstacleLayer(map_grid::f_point origin, map_grid::i_point map_bounds,
                float resolution, std::string frame);

  ~ObstacleLayer();
  void initialise() override;

  void update_local(float delta_x, float delta_y) override;

private:
  void incoming_pcl(sensor_msgs::msg::PointCloud2::SharedPtr,
                    std::shared_ptr<ObservationBuffer>);
  void spin_loop();
  void raytrace_free_space(const Observation *clearing_observation);

  void raytrace_line(map_grid::i_point p0, map_grid::i_point p1,
                     uint32_t max_length);

  void bresenham(uint32_t abs_da, uint32_t abs_db, int error_b, int offset_a,
                 int offset_b, int offset, uint32_t max_length);

  rclcpp::Clock ros_clock_;
  std::string global_frame_;
  tf2_ros::Buffer *tf_buffer_;
  std::vector<std::shared_ptr<ObservationBuffer>> observation_buffers_,
      marking_buffers_, clearing_buffers_;
  std::vector<rclcpp::Subscription<sensor_msgs::msg::PointCloud2>::SharedPtr>
      observation_subscriptions_;
};
} // namespace costmap
} // namespace bROS
#endif // COSTMAP_OBSTACLE_LAYER_H_
