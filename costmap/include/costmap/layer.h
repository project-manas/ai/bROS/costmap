#ifndef COSTMAP_LAYER_H_
#define COSTMAP_LAYER_H_

#include <cmath>
#include <nav_msgs/msg/odometry.hpp>
#include <rclcpp/rclcpp.hpp>
#include <string>

#include "map_grid/map_grid.h"

#ifdef NOISY
#define LOG(str) std::cerr << str << std::endl;
#else
#define LOG(str) ;
#endif

namespace bROS {

namespace costmap {

class Layer {
public:
  // allocate the map here
  Layer(map_grid::f_point origin, map_grid::i_point map_bounds,
        float resolution, std::string frame);

  // start subscription services here
  virtual void initialise();

  virtual void update_local(float delta_x,
                            float delta_y) = 0; // pure virtual function
  void update_global(std::shared_ptr<map_grid::MapGrid> global_map);

protected:
  std::shared_ptr<map_grid::MapGrid> map_;

  bool is_init_;
};

} // namespace costmap

} // namespace bROS

#endif // COSTMAP_LAYER_H_
