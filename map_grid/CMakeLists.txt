cmake_minimum_required(VERSION 3.5)
project(map_grid)

set(CMAKE_CXX_STANDARD 17)
add_compile_options(-march=native -Wall -Wextra -funroll-all-loops -g)

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(nav_msgs REQUIRED)

set(dependencies
    rclcpp nav_msgs)
add_definitions(-DNOISY) # enable for debugging

include_directories(include)

add_library(map_grid SHARED src/map_grid.cpp src/aligned_realloc.cpp)
ament_target_dependencies(map_grid ${dependencies})
install(TARGETS map_grid
    ARCHIVE DESTINATION lib
    LIBRARY DESTINATION lib
    RUNTIME DESTINATION bin)

add_executable(map_grid_tests tests/map_grid_tests.cpp)
target_link_libraries(map_grid_tests map_grid)

add_executable(map_grid_publisher tests/map_grid_publisher.cpp)
target_link_libraries(map_grid_publisher map_grid)
ament_target_dependencies(map_grid_publisher ${dependencies})

add_executable(map_grid_subscriber tests/map_grid_subscriber.cpp)
target_link_libraries(map_grid_subscriber map_grid)
ament_target_dependencies(map_grid_subscriber ${dependencies})

install(TARGETS
    map_grid_publisher
    map_grid_subscriber
    DESTINATION lib/${PROJECT_NAME})

install(DIRECTORY include/
    DESTINATION include/)

ament_export_include_directories(include)
ament_export_libraries(map_grid)
ament_export_dependencies(${dependencies})

ament_package()
