//
// Created by squadrick on 9th November 2018.
//

#include "map_grid/aligned_realloc.h"

#ifdef __APPLE__
#define _malloc_size malloc_size
#else
#define _malloc_size malloc_usable_size
#endif

void *aligned_realloc(void *ptr, size_t alignment, size_t size) {
  if (!ptr)
    return std::aligned_alloc(alignment, size);

  size_t usable = _malloc_size(ptr);

  if (!((uintptr_t)ptr & (alignment - 1)))
    if (usable >= size)
      return ptr;

  void *new_ptr = std::aligned_alloc(alignment, size);

  if (!new_ptr)
    return nullptr;

  memcpy(new_ptr, ptr, usable < size ? usable : size);
  free(ptr);

  return new_ptr;
}
