#include <map_grid/map_grid.h>

#include "map_grid/map_grid.h"

namespace bROS {

namespace map_grid {

MapGrid::MapGrid()
    : origin_({0, 0}), map_bounds_({0, 0}), resolution_(1.0), map_(nullptr),
      x_delta_(0.0), y_delta_(0.0) {}

MapGrid::MapGrid(f_point origin, i_point map_bounds, float resolution,
                 std::string frame)
    : origin_(origin), map_bounds_(map_bounds), resolution_(resolution),
      frame_(frame), x_delta_(0.0), y_delta_(0.0) {
  map_ = _map_alloc(map_bounds_);
}

MapGrid::MapGrid(const MapGrid &other) {
  origin_ = other.origin_;
  map_bounds_ = other.map_bounds_;
  resolution_ = other.resolution_;
  map_ = other.map_;
  frame_ = other.frame_;
  x_delta_ = other.x_delta_;
  y_delta_ = other.y_delta_;
}

MapGrid::~MapGrid() { _map_free(map_); }

int32_t MapGrid::get_height() const { return map_bounds_.x; }

int32_t MapGrid::get_width() const { return map_bounds_.y; }

int32_t MapGrid::get_index(int x, int y) const {
  return static_cast<int32_t>(x * map_bounds_.y + y);
}

int8_t &MapGrid::operator[](int32_t idx) const {
  if (idx >= size()) {
    LOG("Out of bounds access");
    throw std::out_of_range("Trying to access cells outside the data area");
  }

  return map_[idx];
}

int8_t *MapGrid::get_map() { return map_; }

void MapGrid::set_all(uint8_t constant) {
  LOG("Setting entire map to " << static_cast<uint32_t>(constant));
  std::memset(map_, constant, _aligned_size(map_bounds_));
}

void MapGrid::update_with_avg(const MapGrid &other) {
  if (!_dim_match(other))
    throw std::length_error("update_with_max(): Sizes of maps don't match");

  auto *this_map_256 = reinterpret_cast<__m256i *>(map_);
  auto *other_map_256 = reinterpret_cast<__m256i *>(other.map_);

  for (size_t i = 0; i < _aligned_size(map_bounds_) / SIMD_VEC_SIZE; ++i)
    this_map_256[i] = _mm256_avg_epu8(this_map_256[i], other_map_256[i]);
}

void MapGrid::update_with_max(const MapGrid &other) {
  if (!_dim_match(other))
    throw std::length_error("update_with_max(): Sizes of maps don't match");

  auto *this_map_256 = reinterpret_cast<__m256i *>(map_);
  auto *other_map_256 = reinterpret_cast<__m256i *>(other.map_);

  for (size_t i = 0; i < _aligned_size(map_bounds_) / SIMD_VEC_SIZE; ++i)
    this_map_256[i] = _mm256_max_epi8(this_map_256[i], other_map_256[i]);
}

void MapGrid::reset() {
  set_all(0);
  x_delta_ = 0.0;
  y_delta_ = 0.0;
}

int32_t MapGrid::size() const { return map_bounds_.x * map_bounds_.y; }

f_point MapGrid::map_to_world(int x, int y) const {
  f_point world_point = {(x * resolution_) + origin_.x,
                         (y * resolution_) + origin_.y};
  return world_point;
}

void MapGrid::_display_raw() const {
  _display_info();
  for (auto i = 0; i < map_bounds_.x; ++i) {
    for (auto j = 0; j < map_bounds_.y; ++j)
      std::cout << static_cast<int>(map_[get_index(i, j)]) << "\t";
    std::cout << "\n";
  }
}

void MapGrid::_display_info() const {
  std::cout << "Origin: (" << origin_.x << ", " << origin_.y << ") \n";
  std::cout << "Resolution: " << resolution_ << std::endl;
  std::cout << "Cell Bounds: (" << map_bounds_.x << ", " << map_bounds_.y
            << ") \n";
  auto world_point = map_to_world(map_bounds_.x, map_bounds_.y);
  std::cout << "Map Bounds: (" << world_point.x << ", " << world_point.y
            << ") \n";
  std::cout << "Frame: " << frame_ << "\n";
}

int8_t *_map_alloc(i_point map_bounds, bool init) {
  auto size = _aligned_size(map_bounds);
  auto map = reinterpret_cast<int8_t *>(aligned_alloc(SIMD_VEC_SIZE, size));
  if (init)
    std::memset(map, -1, static_cast<size_t>(size));

  return map;
}

size_t _aligned_size(i_point map_bounds) {
  auto size = map_bounds.y * map_bounds.x;
#define SAFETY_ZONE 128 // To prevent buffer overflow segfaults
  return static_cast<size_t>(SIMD_VEC_SIZE * (1 + (size / SIMD_VEC_SIZE)) +
                             SAFETY_ZONE);
#undef SAFETY_ZONE
}

void _map_free(void *map_ptr) {
  if (map_ptr != nullptr)
    free(map_ptr);
  map_ptr = nullptr;
}

bool MapGrid::_dim_match(const map_grid::MapGrid &other) const {
  return origin_.x == other.origin_.x && origin_.y == other.origin_.y &&
         resolution_ == other.resolution_ &&
         map_bounds_.x == other.map_bounds_.x &&
         map_bounds_.y == other.map_bounds_.y;
}

void MapGrid::resize(const i_point new_bounds) {
  if (new_bounds.x == map_bounds_.x && new_bounds.y == map_bounds_.y) {
    LOG("Resizing to the same size");
    return;
  }

  auto map = _map_alloc(new_bounds);

  int new_height = std::min(new_bounds.x, map_bounds_.x);
  int new_width = std::min(new_bounds.y, map_bounds_.y);

  for (int i = 0; i < new_height; ++i) {
    std::memcpy(map + i * new_bounds.y, map_ + i * map_bounds_.y,
                static_cast<size_t>(new_width));
  }

  _map_free(map_);
  map_ = map;
  map_bounds_ = new_bounds;
}

static float one_d_interpolate(float s, float e, float t) {
  return s + (e - s) * t;
}

static float two_d_interpolate(int8_t c00, int8_t c01, int8_t c10, int8_t c11,
                               float tx, float ty) {
  return one_d_interpolate(one_d_interpolate(c00, c01, tx),
                           one_d_interpolate(c10, c11, tx), ty);
}

void MapGrid::interpolate(int8_t *new_map, int8_t *old_map, i_point new_bounds,
                          i_point old_bounds) {
  //  This is much slower (x3) than interpolate_fast, but it less prone to
  //  numerical instablity Use it for slower but more guaranteed stable results
  for (int x = 0, y = 0; x < new_bounds.x; y++) {
    if (y >= new_bounds.y) {
      y = 0;
      x++;
    }
    float gx = x / (float)(new_bounds.x) * (old_bounds.x);
    float gy = y / (float)(new_bounds.y) * (old_bounds.y);
    int gxi = (int)gx;
    int gyi = (int)gy;
    int8_t result = 0;
    int8_t c00 = old_map[get_index(gxi, gyi)];
    int8_t c10 = old_map[get_index(gxi + 1, gyi)];
    int8_t c01 = old_map[get_index(gxi, gyi + 1)];
    int8_t c11 = old_map[get_index(gxi + 1, gyi + 1)];

    result = static_cast<int8_t>(
        two_d_interpolate(c00, c01, c10, c11, gy - gyi, gx - gxi));
    new_map[x * new_bounds.y + y] = result;
  }
}

static inline __m256 _mm256_mod_ps(__m256 a, __m256 b, __m256 c) {
  __m256 base = _mm256_mul_ps(b, c);
  return _mm256_sub_ps(a, base);
}

static inline __m256 _mm256_div_idx_ps(__m256 a, __m256 b_rcp) {
  __m256 c = _mm256_mul_ps(a, b_rcp);
  return _mm256_round_ps(c, _MM_FROUND_TO_ZERO | _MM_FROUND_NO_EXC);
}

static inline __m256 _mm256_div_idx_ps_norcp(__m256 a, __m256 b) {
  //  This is slower than the function above, but it is more numerically
  //  unstable. If the interpolation is slightly off-position, switch to this
  //  function.
  __m256 c = _mm256_div_ps(a, b);
  return _mm256_round_ps(c, _MM_FROUND_TO_ZERO | _MM_FROUND_NO_EXC);
}

static inline __m256 _mm256_get_index(__m256 x, __m256 y, __m256 width) {
  return _mm256_fmadd_ps(x, width, y);
}

template <typename T, int N, typename V> static void printVec(V vec) {
  //  For debugging SIMD Vectors
  //  printVec<float, 8>(a); // type of a is __m256
  //  printVec<int, 8>(b); // type of b is __m256i
  //  printVec<uint8_t, 64>(c); // type of c is __m256i
  T *iter = reinterpret_cast<T *>(&vec);
  for (int i = 0; i < N; ++i)
    std::cout << iter[i] << "\t";
  std::cout << std::endl;
}

static __m256 _mm256_one_d_interpolate(__m256 s, __m256 e, __m256 t) {
  return _mm256_add_ps(s, _mm256_mul_ps(_mm256_sub_ps(e, s), t));
}

static __m256 _mm256_two_d_interpolate(__m256 c00, __m256 c01, __m256 c10,
                                       __m256 c11, __m256 tx, __m256 ty) {
  return _mm256_one_d_interpolate(_mm256_one_d_interpolate(c00, c01, tx),
                                  _mm256_one_d_interpolate(c10, c11, tx), ty);
}

void MapGrid::interpolate_fast(int8_t *new_map, int8_t *old_map,
                               i_point new_bounds, i_point old_bounds) const {
// TODO (Squadrick): Consider switching to dvec.h
#define INDICES_SIZE 8

  const float width_ratio = static_cast<float>(old_bounds.y) / new_bounds.y;
  const float height_ratio = static_cast<float>(old_bounds.x) / new_bounds.x;

  const __m256 one_vec = _mm256_set1_ps(1.0f);
  const __m256 width_vec = _mm256_set1_ps((float)new_bounds.y);
  const __m256 width_vec_rcp = _mm256_div_ps(_mm256_set1_ps(1.0f), width_vec);
  const __m256 old_width_vec = _mm256_set1_ps((float)old_bounds.y);

  for (int32_t x = 0; x < new_bounds.y * new_bounds.x - INDICES_SIZE + 1;
       x += INDICES_SIZE) {
    __m256 idxs = _mm256_set_ps(x + 7.0f, x + 6.0f, x + 5.0f, x + 4.0f,
                                x + 3.0f, x + 2.0f, x + 1.0f, x + 0.0f);
    __m256 xs = _mm256_div_idx_ps(idxs, width_vec_rcp);
    __m256 ys = _mm256_mod_ps(idxs, width_vec, xs);

    __m256 gx = _mm256_mul_ps(xs, _mm256_set1_ps(height_ratio));
    __m256 gy = _mm256_mul_ps(ys, _mm256_set1_ps(width_ratio));
    __m256 gxr = _mm256_round_ps(gx, _MM_FROUND_TO_NEG_INF | _MM_FROUND_NO_EXC);
    __m256 gyr = _mm256_round_ps(gy, _MM_FROUND_TO_NEG_INF | _MM_FROUND_NO_EXC);

    __m256 gxr_p1 = _mm256_add_ps(gxr, one_vec);
    __m256 gyr_p1 = _mm256_add_ps(gyr, one_vec);

    __m256 idxs00 = _mm256_get_index(gxr, gyr, old_width_vec);
    __m256 idxs10 = _mm256_get_index(gxr_p1, gyr, old_width_vec);
    __m256 idxs01 = _mm256_get_index(gxr, gyr_p1, old_width_vec);
    __m256 idxs11 = _mm256_get_index(gxr_p1, gyr_p1, old_width_vec);

    auto *i00 = reinterpret_cast<float *>(&idxs00);
    auto *i01 = reinterpret_cast<float *>(&idxs01);
    auto *i10 = reinterpret_cast<float *>(&idxs10);
    auto *i11 = reinterpret_cast<float *>(&idxs11);

    __m256 c_vec00, c_vec01, c_vec10, c_vec11;
    auto *c00 = reinterpret_cast<float *>(&c_vec00);
    auto *c01 = reinterpret_cast<float *>(&c_vec01);
    auto *c10 = reinterpret_cast<float *>(&c_vec10);
    auto *c11 = reinterpret_cast<float *>(&c_vec11);

    for (int i = 0; i < INDICES_SIZE; ++i) {
      c00[i] = static_cast<float>(old_map[(int)i00[i]]);
      c01[i] = static_cast<float>(old_map[(int)i01[i]]);
      c10[i] = static_cast<float>(old_map[(int)i10[i]]);
      c11[i] = static_cast<float>(old_map[(int)i11[i]]);
    }

    __m256 tx = _mm256_sub_ps(gx, gxr);
    __m256 ty = _mm256_sub_ps(gy, gyr);

    __m256 interpolated_values =
        _mm256_two_d_interpolate(c_vec00, c_vec01, c_vec10, c_vec11, ty, tx);
    __m256 new_indices = _mm256_get_index(xs, ys, width_vec);

    auto *new_index = reinterpret_cast<float *>(&new_indices);
    auto *interpolated_value = reinterpret_cast<float *>(&interpolated_values);

    for (int i = 0; i < INDICES_SIZE; ++i)
      new_map[static_cast<int>(new_index[i])] =
          static_cast<uint8_t>(interpolated_value[i]);
  }
#undef INDICES_SIZE
}

void MapGrid::change_resolution(float resolution, bool fast) {
  if (FEQ(resolution, 0.0f))
    throw std::range_error("Resolution can't be zero or very small");

  if (FEQ(resolution, resolution_)) {
    LOG("Trying to change to the same resolution");
    return;
  }

  i_point new_bounds = {static_cast<int>(C_TO_M(map_bounds_.x) / resolution),
                        static_cast<int>(C_TO_M(map_bounds_.y) / resolution)};

  auto new_map = _map_alloc(new_bounds);

  if (fast) {
    LOG("Using fast interpolation for changing resolution");
    interpolate_fast(new_map, map_, new_bounds, map_bounds_);
  } else {
    LOG("Using slow interpolation for changing resolution");
    interpolate(new_map, map_, new_bounds, map_bounds_);
  }

  if (map_ == nullptr) {
    LOG("FUCKED");
  }
  _map_free(map_);
  map_ = new_map;
  resolution_ = resolution;
  map_bounds_ = new_bounds;
}

void MapGrid::fromROSMsg(std::shared_ptr<nav_msgs::msg::OccupancyGrid> grid,
                         bool align) {
  origin_.x = grid->info.origin.position.x;
  origin_.y = grid->info.origin.position.y;
  resolution_ = grid->info.resolution;
  map_bounds_.x = grid->info.height;
  map_bounds_.y = grid->info.width;
  frame_ = grid->header.frame_id;

  _map_free(map_);
  if (align) {
    map_ = _map_alloc(map_bounds_, false);
    std::memcpy(map_, grid->data.data(), _aligned_size(map_bounds_));
  } else {
    map_ = grid->data.data();
  }
}

void MapGrid::toROSMsg(nav_msgs::msg::OccupancyGrid &grid) {
  grid.info.origin.position.x = origin_.y;
  grid.info.origin.position.y = origin_.x;

  grid.info.origin.orientation.x = 0;
  grid.info.origin.orientation.y = 0;
  grid.info.origin.orientation.z = 0;
  grid.info.origin.orientation.w = 1;

  grid.info.resolution = resolution_;
  grid.info.width = map_bounds_.y;
  grid.info.height = map_bounds_.x;
  grid.header.frame_id = frame_;

  grid.data = std::vector<int8_t>(map_, map_ + size());
}

i_point MapGrid::world_to_map(f_point world_point) const {
  return world_to_map(world_point.x, world_point.y);
}

f_point MapGrid::map_to_world(i_point map_point) const {
  return map_to_world(map_point.x, map_point.y);
}

} // namespace map_grid

} // namespace bROS
