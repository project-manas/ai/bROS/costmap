//
// Created by squadrick on 12/7/18.
//

#include <nav_msgs/msg/occupancy_grid.hpp>
#include <rclcpp/rclcpp.hpp>

#include "map_grid/map_grid.h"

class MapGridPublisher : public rclcpp::Node {
public:
  MapGridPublisher()
      : Node("map_grid_publisher"), map_grid_({0, 0}, {100, 200}, 0.01) {
    count = 0;
    move_x = 0.2;
    move_y = 0.1;
    map_grid_.set_all(0);

    for (int i = 20; i < 70; ++i) {
      for (int j = 30; j < 80; ++j) {
        map_grid_[map_grid_.get_index(i, j)] = 100;
      }
    }

    publisher_ = this->create_publisher<nav_msgs::msg::OccupancyGrid>("/map");

    RCLCPP_INFO(this->get_logger(), "MapGridPublisher created");
  }

  void publish_map() {
    count++;
    auto message = nav_msgs::msg::OccupancyGrid();
    map_grid_.toROSMsg(message);
    map_grid_.move(move_x, move_y);
    message.header.stamp = this->get_clock()->now();
    publisher_->publish(message);

    rclcpp::sleep_for(std::chrono::seconds(2));

    if (count % 5 == 0 && count % 10 != 0)
      move_x *= -1;
    if (count % 10 == 0)
      move_y *= -1;
  }

private:
  bROS::map_grid::MapGrid map_grid_;
  rclcpp::Publisher<nav_msgs::msg::OccupancyGrid>::SharedPtr publisher_;
  uint32_t count;

  float move_x, move_y;
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<MapGridPublisher>();
  while (rclcpp::ok()) {
    node->publish_map();
  }
  rclcpp::shutdown();
  return 0;
}
