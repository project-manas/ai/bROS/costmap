#define CATCH_CONFIG_MAIN

#include "map_grid/map_grid.h"
#include <catch.hpp>

namespace bROS {
class AreaContains : public Catch::MatcherBase<map_grid::MapGrid> {
  map_grid::i_point start_, end_;
  uint8_t elem_;

public:
  AreaContains(map_grid::i_point start, map_grid::i_point end, uint8_t elem)
      : start_(start), end_(end), elem_(elem) {}

  bool match(const map_grid::MapGrid &map) const override {
    bool cond = true;
    for (int i = start_.x; i < end_.x; i++)
      for (int j = start_.y; j < end_.y; j++)
        cond = cond && (map[map.get_index(i, j)] == elem_);
    return cond;
  }

  std::string describe() const override {
    std::ostringstream ss;
    ss << " has area between (" << start_.x << ", " << start_.y << ") and ("
       << end_.x << ", " << end_.y
       << ") with only value: " << static_cast<int32_t>(elem_);
    return ss.str();
  }
};

class OnlyContains : public Catch::MatcherBase<map_grid::MapGrid> {
  uint8_t elem_;

public:
  explicit OnlyContains(uint8_t elem) : elem_(elem) {}

  bool match(const map_grid::MapGrid &map) const override {
    bool cond = true;
    for (int32_t i = 0; i < map.size(); ++i)
      cond = cond && (map[i] == elem_);
    return cond;
  }

  std::string describe() const override {
    std::ostringstream ss;
    ss << " contains only element: " << static_cast<int32_t>(elem_);
    return ss.str();
  }
};

class SameAs : public Catch::MatcherBase<map_grid::MapGrid> {
  map_grid::MapGrid &map_;

public:
  explicit SameAs(map_grid::MapGrid &map) : map_(map) {}

  bool match(const map_grid::MapGrid &map) const override {
    if (map.size() != map_.size())
      return false;

    bool cond = true;
    for (int i = 0; i < map.size(); ++i)
      cond = cond && (map[i] == map_[i]);

    return cond;
  }
  std::string describe() const override {
    std::ostringstream ss;
    ss << " blah blah";
    return ss.str();
  }
};

TEST_CASE("Map grid basics", "[map_grid_basic]") {
  map_grid::MapGrid map({0, 0}, {100, 150}, 0.1);

  REQUIRE(map.origin_.x == Approx(0));
  REQUIRE(map.origin_.y == Approx(0));
  REQUIRE(map.get_height() == 100);
  REQUIRE(map.get_width() == 150);

  REQUIRE_THROWS(map[map.size() + 2]);

  REQUIRE(map.size() == (100 * 150));
  REQUIRE(map.get_index(0, 0) == 0);
  REQUIRE(map.get_index(map.get_height() - 1, map.get_width() - 1) ==
          map.size() - 1);

  REQUIRE(map.check_bounds(5, 7.1));
  REQUIRE(!map.check_bounds(12.31, 21.36));
  REQUIRE(!map.check_bounds(11.1, 3));
  REQUIRE(!map.check_bounds(3.99, 6969));

  int sum = 0;
  map.set_all(0);
  for (auto i = 0; i < map.size(); ++i)
    sum += map[i];
  REQUIRE(sum == 0);

  map.set_all(3);
  for (auto i = 0; i < map.size(); ++i)
    sum += map[i];
  REQUIRE(sum == 3 * map.size());

  map.set_origin(-12.2, 10);
  REQUIRE(map.origin_.x == Approx(-12.2));
  REQUIRE(map.origin_.y == Approx(10));

  map.set_origin(0, 0);
  REQUIRE(map.origin_.x == Approx(0));
  REQUIRE(map.origin_.y == Approx(0));

  auto world_point = map.map_to_world(79, 83);
  REQUIRE(world_point.x == Approx(7.9));
  REQUIRE(world_point.y == Approx(8.3));

  map.set_origin(1, -2);
  auto map_point = map.world_to_map(7, 8.3);
  REQUIRE(map_point.x == 60);
  REQUIRE(map_point.y == 103);

  map.resize(1.2, 3.5);
  REQUIRE(map.map_bounds_.x == 12);
  REQUIRE(map.map_bounds_.y == 35);
}

TEST_CASE("Changing resolution", "[map_grid_resolution]") {
  map_grid::MapGrid map1({0, 0}, {10, 5}, 0.1);
  map_grid::MapGrid map2({0, 0}, {10, 5}, 0.1);

  for (int i = 2; i < 8; ++i) {
    for (int j = 1; j < 4; ++j) {
      map1[map1.get_index(i, j)] = 100;
      map2[map2.get_index(i, j)] = 100;
    }
  }

  map1.change_resolution(0.05);
  REQUIRE(map1.resolution_ == Approx(0.05));
  REQUIRE(map1.map_bounds_.x == 20);
  REQUIRE(map1.map_bounds_.y == 10);

  map2.change_resolution(0.05, false);
  REQUIRE(map2.resolution_ == Approx(0.05));
  REQUIRE(map2.map_bounds_.x == 20);
  REQUIRE(map2.map_bounds_.y == 10);

  REQUIRE_THAT(map1, SameAs(map2));
}

TEST_CASE("Moving a map around", "[map_grid_move]") {
  map_grid::MapGrid map({0, 0}, {10, 5}, 0.1);

  for (int i = 2; i < 8; ++i) {
    for (int j = 1; j < 4; ++j) {
      map[map.get_index(i, j)] = 100;
    }
  }

  // TODO (squadrick): Fix the test cases for map moving
  SECTION("Positive X, Positive Y") {
    map.move(0.2, 0.3);

    //    REQUIRE_THAT(map, AreaContains({0, 0}, {map.get_height(),
    //    map.get_width() - 1}, 0)); REQUIRE_THAT(map, AreaContains({0, 4}, {3,
    //    4}, 0)); REQUIRE_THAT(map, AreaContains({4, 4}, {map.get_width(), 4},
    //    100));
  }

  SECTION("Negative X, Negative Y") {
    map.move(-0.2, -0.3);

    //    REQUIRE_THAT(map, AreaContains({0, 1}, {map.get_height(),
    //    map.get_width()}, 0)); REQUIRE_THAT(map, AreaContains({0, 0}, {5, 0},
    //    100)); REQUIRE_THAT(map, AreaContains({6, 0}, {map.get_height(), 0},
    //    0));
  }

  SECTION("Out of bounds") {
    map.move(100, -100);
    //    REQUIRE_THAT(map, OnlyContains(0));
  }
}

TEST_CASE("Actions between maps", "[map_grid_interaction]") {
  map_grid::MapGrid map1({0, 0}, {100, 100}, 0.1);
  map_grid::MapGrid map2({0, 0}, {100, 100}, 0.1);

  map1.set_all(100);
  map2.set_all(0);

  SECTION("Max") {
    map2.update_with_max(map1);
    REQUIRE_THAT(map2, OnlyContains(100));
  }

  SECTION("Average") {
    map2.update_with_avg(map1);
    REQUIRE_THAT(map2, OnlyContains(50));
  }
}

} // namespace bROS
