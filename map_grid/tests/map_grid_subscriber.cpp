//
// Created by squadrick on 12/7/18.
//

#include <nav_msgs/msg/occupancy_grid.hpp>
#include <rclcpp/rclcpp.hpp>

#include "map_grid/map_grid.h"

class MapGridSubscriber : public rclcpp::Node {
public:
  MapGridSubscriber() : Node("map_grid_subscriber") {
    RCLCPP_INFO(this->get_logger(), "MapGridSubscriber created");
    subscription_ = this->create_subscription<nav_msgs::msg::OccupancyGrid>(
        "/map", std::bind(&MapGridSubscriber::topic_callback, this,
                          std::placeholders::_1));
  }

private:
  void topic_callback(const nav_msgs::msg::OccupancyGrid::SharedPtr msg) {
    RCLCPP_INFO(this->get_logger(), "Subscribing to %s",
                subscription_->get_topic_name());
    map.fromROSMsg(msg);
    map._display_info();
  }
  bROS::map_grid::MapGrid map;
  rclcpp::Subscription<nav_msgs::msg::OccupancyGrid>::SharedPtr subscription_;
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  auto node = std::make_shared<MapGridSubscriber>();
  rclcpp::spin(node);
  rclcpp::shutdown();
}
