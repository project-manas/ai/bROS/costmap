//
// Created by squadrick on 9th November 2018.
//

#ifndef map_grid_ALIGNED_REALLOC_H
#define map_grid_ALIGNED_REALLOC_H

#include <cstdint>
#include <cstdlib>
#include <cstring>
#ifdef __linux__
#include <malloc.h>
#endif

void *aligned_realloc(void *ptr, size_t alignment, size_t size);

#endif // map_grid_ALIGNED_REALLOC_H
