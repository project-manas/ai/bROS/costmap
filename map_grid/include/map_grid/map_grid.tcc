//
// Created by dheeraj98reddy@gmail.com on 11th November 2018
//

#ifndef map_grid_MAP_GRID_TCC
#define map_grid_MAP_GRID_TCC

ENSURE_NUMERIC_IMPL(T, V)
void MapGrid::resize(T x, V y) {
  i_point new_bounds = {M_TO_C(x), M_TO_C(y)};
  resize(new_bounds);
}

ENSURE_NUMERIC_IMPL(T, V)
void MapGrid::shift(T x, V y) {
  LOG("Shifting origin");
  origin_.x += x;
  origin_.y += y;
}

ENSURE_NUMERIC_IMPL(T, V)
void MapGrid::move(T x, V y) {
  shift(x, y);

  if (std::abs(M_TO_C(x)) >= map_bounds_.x ||
      std::abs(M_TO_C(y)) >= map_bounds_.y) {
    set_all(0);
    LOG("Moving map with no overlap. Set all to 0.");
    return;
  }

  int x_diff = std::abs(std::abs(M_TO_C(x)) - map_bounds_.x);
  int y_diff = std::abs(std::abs(M_TO_C(y)) - map_bounds_.y);
  LOG("X-diff: " << x_diff << ", Y-diff: " << y_diff);

  i_point old_bounds = {x > 0 ? 0 : M_TO_C(-x), y > 0 ? 0 : M_TO_C(-y)};
  i_point new_bounds = {x > 0 ? M_TO_C(x) : 0, y > 0 ? M_TO_C(y) : 0};
  LOG("Old bounds: " << old_bounds.x << ", " << old_bounds.y);
  LOG("New bounds: " << new_bounds.x << ", " << new_bounds.y);

  auto map = _map_alloc(map_bounds_);

  for (int i = 0; i < x_diff; ++i) {
    std::memcpy(map + get_index(old_bounds.x + i, old_bounds.y),
                map_ + get_index(new_bounds.x + i, new_bounds.y),
                static_cast<size_t>(y_diff));
  }
  _map_free(map_);
  this->map_ = map;
}

ENSURE_NUMERIC_IMPL(T, V)
bool MapGrid::check_bounds(T x, V y) const {
  auto transformed_point = world_to_map(x, y);

  return !(transformed_point.x < 0 || transformed_point.x > map_bounds_.x ||
           transformed_point.y < 0 || transformed_point.y > map_bounds_.y);
}

ENSURE_NUMERIC_IMPL(T, V)
i_point MapGrid::world_to_map(T x, V y) const {
  auto map_x = M_TO_C(x - origin_.x);
  auto map_y = M_TO_C(y - origin_.y);
  return {map_x, map_y};
}

ENSURE_NUMERIC_IMPL(T, V)
void MapGrid::set_origin(T x, V y) {
  origin_ = {static_cast<float>(x), static_cast<float>(y)};
}

#endif // map_grid_MAP_GRID_TCC
