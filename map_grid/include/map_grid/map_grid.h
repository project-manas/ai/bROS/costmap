//
// Created by dheeraj98reddy@gmail on 6th November 2018
//

#ifndef map_grid_MAP_GRID_H
#define map_grid_MAP_GRID_H

#include <cmath>
#include <cstring>
#include <ctime>
#include <iostream>
#include <memory>
#include <mutex>
#include <stdexcept>
#include <vector>

#include <immintrin.h>

#include "map_grid/aligned_realloc.h"
#include "nav_msgs/msg/occupancy_grid.hpp"

#define TIME_START                                                             \
  std::clock_t start;                                                          \
  start = std::clock();
#define TIME_END(label)                                                        \
  std::cout << label << " takes "                                              \
            << (std::clock() - start) / (CLOCKS_PER_SEC / 1000.0) << " ms"     \
            << std::endl;

/// \brief Use for alignment of the underlying map_, 32 because 256-bits/8-bits
/// = 32
#define SIMD_VEC_SIZE 32

/// \brief Ensures that both T and V are numeric -- signed and unsigned ints,
/// float and double
#define ENSURE_NUMERIC(T, V)                                                   \
  template <                                                                   \
      typename T, typename V,                                                  \
      typename =                                                               \
          typename std::enable_if<std::is_arithmetic<T>::value, T>::type,      \
      typename =                                                               \
          typename std::enable_if<std::is_arithmetic<V>::value, V>::type>
#define ENSURE_NUMERIC_IMPL(T, V)                                              \
  template <typename T, typename V, typename, typename>

/// \brief M->Map, C->Cell. Used for scaling.
#define M_TO_C(point) static_cast<int>(std::rint((point) / resolution_))
#define C_TO_M(point) (point) * resolution_

/// \brief Check for floating point equality
// TODO (Squadrick): Use relative difference
// (https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
#define FEQ(x, y) std::fabs((x) - (y)) < 1e-5

/// \brief Logging useful information while debugging, active only when `NOISY`
/// flag is set
#ifdef NOISY
#define LOG(str) std::cerr << str << std::endl;
#else
#define LOG(str) ;
#endif

namespace bROS {

namespace map_grid {

struct f_point {
  float x, y;
};
struct i_point {
  int x, y;
};

size_t _aligned_size(i_point);
int8_t *_map_alloc(i_point, bool init = true);
void _map_free(void *map_ptr);

class MapGrid {

public:
  f_point origin_;
  i_point map_bounds_;
  float resolution_;
  std::string frame_;
  std::mutex map_lock_;

  MapGrid();
  MapGrid(const MapGrid &other);
  MapGrid(f_point origin, i_point map_bounds, float resolution,
          std::string frame = "map");
  ~MapGrid();

  /// \brief Converts a cell point to the world frame
  /// \param x Integer cell coordinate of x-axis
  /// \param y Integer cell coordinate of y-axis
  /// \return Transformed world point
  f_point map_to_world(int x, int y) const;
  f_point map_to_world(i_point map_point) const;

  /// \brief Get the visible size of the grid
  int32_t size() const;

  /// \brief Get the height
  int32_t get_height() const;

  /// \brief Get the width
  int32_t get_width() const;

  /// \brief Convert 2-D cell coordinate into 1-D index
  /// \param x Integer cell coordinate of x-axis
  /// \param y Integer cell coordinate of y-axis
  /// \return 1-D index for accessing the grid
  int32_t get_index(int x, int y) const;

  /// \brief Access a grid element
  /// \param idx 1-D index value
  /// \return Reference to the corresponding grid cell
  int8_t &operator[](int32_t idx) const;

  /// \brief Get the underlying raw pointer of the map
  int8_t *get_map();

  /// \brief Change the resolution of the map with the values retained
  void change_resolution(float resolution, bool fast = true);

  /// \brief Expand or decrease the area of the map, does not work like an image
  /// resize. For see use `change_resolution` \param new_bounds The new map
  /// bounds
  void resize(i_point new_bounds);

  /// \brief Reset the map
  void reset();

  /// \brief Set the value of all cells of the map to a given value
  void set_all(uint8_t constant);

  /// \brief Average each cell's value with the corresponding cell in `other`
  /// (Uses AVX2) \param other Reference to a MapGrid object
  void update_with_avg(const MapGrid &other);

  /// \brief Update each cell's value to the max of this map or the
  /// corresponding cell in `other` (Uses AVX2) \param other Reference to a
  /// MapGrid object
  void update_with_max(const MapGrid &other);

  /// \brief Bilinear interpolation used in `change_resolution`
  void interpolate(int8_t *, int8_t *, i_point, i_point);

  /// \brief Bilinear interpolation used in `change_resolution` (Uses AVX2)
  void interpolate_fast(int8_t *, int8_t *, i_point, i_point) const;

  void fromROSMsg(std::shared_ptr<nav_msgs::msg::OccupancyGrid> grid,
                  bool align = false);

  void toROSMsg(nav_msgs::msg::OccupancyGrid &grid);

  void lock() { map_lock_.lock(); }

  void unlock() { map_lock_.unlock(); }

  /// \brief Check whether a given point is within the bounds of the map
  /// \param x World coordinate of the point along x-axis
  /// \param y World coordinate of the point along y-axis
  ENSURE_NUMERIC(T, V) bool check_bounds(T x, V y) const;

  /// \brief Convert the world coordinates to the map cell coordinates
  /// \param x World coordinate of the point along x-axis
  /// \param y World coordinate of the point along y-axis
  /// \return Transformed cell coordinate
  ENSURE_NUMERIC(T, V) i_point world_to_map(T x, V y) const;
  i_point world_to_map(f_point world_point) const;
  /// \brief Change the origin of the grid
  /// \param x World coordinate of the point along x-axis
  /// \param y World coordinate of the point along y-axis
  ENSURE_NUMERIC(T, V) void set_origin(T x, V y);

  /// \brief Shift only the origin, the grid values remain the same
  /// \param x Change in world coordinate along x-axis
  /// \param y Change in world coordinate along y-axis
  ENSURE_NUMERIC(T, V) void shift(T x, V y);

  /// \brief Resize a map to the given world coordinates
  /// \param x World coordinate of the point along x-axis
  /// \param y World coordinate of the point along y-axis
  ENSURE_NUMERIC(T, V) void resize(T x, V y);

  /// \brief Move the origin, and the contents of the map
  /// \param x Change in world coordinate along x-axis
  /// \param y Change in world coordinate along y-axis
  ENSURE_NUMERIC(T, V) void move(T x, V y);

  /// \brief Displays the underlying matrix (for debugging)
  void _display_raw() const;

  /// \brief Displays only meta-data of the grid (for debugging)
  void _display_info() const;

private:
  int8_t *map_;
  double x_delta_, y_delta_;

  bool _dim_match(const MapGrid &other) const;
};

/// \brief Implementation of template functions
#include "map_grid.tcc"
} // namespace map_grid

} // namespace bROS
#endif // map_grid_MAP_GRID_H
